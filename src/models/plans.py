"""Plans related models and database functionality"""
from src.models.base import db


class Plan(db.Model):
    """Model class to represent mobile service plans"""
    __tablename__ = "plans"
    id = db.Column(db.String(30), primary_key=True)
    description = db.Column(db.String(200))
    # amount of data available for a given billing cycle
    mb_available = db.Column(db.BigInteger)
    is_unlimited = db.Column(db.Boolean)

    def __repr__(self):  # pragma: no cover
        return (
            f"<{self.__class__.__name__}: {self.id} ({self.description})>"
        )
