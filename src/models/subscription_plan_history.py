from datetime import datetime

from src.models.base import db


class SubscriptionPlanHistory(db.Model):
    """Model class to represent subscription plan history records"""

    __tablename__ = "subscription_plan_history"
    id = db.Column(db.Integer, primary_key=True)
    subscription_id = db.Column(db.Integer, db.ForeignKey("subscriptions.id"), nullable=False)
    plan_id = db.Column(db.String(30), db.ForeignKey("plans.id"), nullable=False)
    start_plan_date = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    end_plan_date = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    created = db.Column(db.TIMESTAMP(timezone=True), nullable=False, default=datetime.utcnow)
    backdated_by_id = db.Column(db.Integer, db.ForeignKey("subscription_plan_history.id"))
