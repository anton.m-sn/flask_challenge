"""Utilities for models to inherit or use"""
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from flask import abort
from http import HTTPStatus


def get_object_or_404(model, mid):
    """Get an object by id or return a 404 not found response

    Args:
        model (object): object's model class
        mid (int): object's id

    Returns:
        object: returned from query

    Raises:
        404: when no objects match the query
        500: when multiple objects returned

    """
    try:
        return model.query.filter(model.id==mid).one()
    except NoResultFound:
        abort(HTTPStatus.NOT_FOUND)
    except MultipleResultsFound:
        abort(HTTPStatus.INTERNAL_SERVER_ERROR)
