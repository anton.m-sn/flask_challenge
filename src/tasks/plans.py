"""Plan related tasks"""
from collections import defaultdict

from celery.utils.log import get_task_logger

from src.celery_app import celery
from src.models.base import db
from src.models.cycles import BillingCycle
from src.models.plans import Plan
from src.models.subscription_plan_history import SubscriptionPlanHistory

log = get_task_logger(__name__)


@celery.task()
def query_subscription_plans(billing_cycle_id, subscription_id=None):
    """Gets subscription plan history records for the given parameters.

    Args:
        billing_cycle_id (int):
        subscription_id (int or None):

    Returns:
        If subscription_id is passed,
            returns list of tuples of the following format:
            (plan_size, start_effective_date, end_effective_date)
        Otherwise,
            returns defaultdict of the following format:
            {plan_size: [subscription_ids]}

    Raises:
        ValueError: if billing cycle not found

    """
    billing_cycle = BillingCycle.query.get(billing_cycle_id)
    if billing_cycle is None:
        raise ValueError(f"Billing cycle with id={billing_cycle_id} not found")
    cycle_start, cycle_end = billing_cycle.start_date, billing_cycle.end_date

    query = db.session.query(
        Plan.mb_available,
        SubscriptionPlanHistory.subscription_id,
        SubscriptionPlanHistory.start_plan_date,
        SubscriptionPlanHistory.end_plan_date,
    ).filter(
        SubscriptionPlanHistory.start_plan_date < cycle_end,
        SubscriptionPlanHistory.end_plan_date > cycle_start,
        SubscriptionPlanHistory.backdated_by_id == None,
        Plan.id == SubscriptionPlanHistory.plan_id,
    )

    if subscription_id is None:
        result = defaultdict(list)
        for row in query:
            result[row.mb_available].append(row.subscription_id)
    else:
        query = query.filter(
            SubscriptionPlanHistory.subscription_id == subscription_id,
        )
        result = [(row.mb_available, row.start_plan_date, row.end_plan_date) for row in query]

    return result
