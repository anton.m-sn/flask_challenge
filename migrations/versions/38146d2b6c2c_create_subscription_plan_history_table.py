"""Create subscription_plan_history table.

Revision ID: 38146d2b6c2c
Revises: b8f44ce6e2d7
Create Date: 2021-07-20 05:19:08.411026

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '38146d2b6c2c'
down_revision = 'b8f44ce6e2d7'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('subscription_plan_history',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('subscription_id', sa.Integer(), nullable=False),
        sa.Column('plan_id', sa.String(length=30), nullable=False),
        sa.Column('start_plan_date', sa.TIMESTAMP(timezone=True), nullable=False),
        sa.Column('end_plan_date', sa.TIMESTAMP(timezone=True), nullable=False),
        sa.Column('created', sa.TIMESTAMP(timezone=True), nullable=False),
        sa.Column('backdated_by_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['plan_id'], ['plans.id'], ),
        sa.ForeignKeyConstraint(['subscription_id'], ['subscriptions.id'], ),
        sa.ForeignKeyConstraint(['backdated_by_id'], ['subscription_plan_history.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('subscription_plan_history')
