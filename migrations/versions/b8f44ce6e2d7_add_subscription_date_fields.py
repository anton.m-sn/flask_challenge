"""Add activation_date and expiry_date fields to subscriptions table.

Revision ID: b8f44ce6e2d7
Revises: 49952fde9c90
Create Date: 2021-07-19 15:51:46.010839

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b8f44ce6e2d7'
down_revision = '49952fde9c90'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('subscriptions', sa.Column('activation_date', sa.TIMESTAMP(timezone=True), nullable=True))
    op.add_column('subscriptions', sa.Column('expiry_date', sa.TIMESTAMP(timezone=True), nullable=True))


def downgrade():
    op.drop_column('subscriptions', 'expiry_date')
    op.drop_column('subscriptions', 'activation_date')
